﻿using System.Collections.Generic;
using KS.Character;
using UnityEngine;
using UnityEngine.AI;

namespace KS.AI.FSM.SampleFSMs.SimpleCharacterAI1
{
    [RequireComponent(typeof(UnityEngine.Animator))]
    [RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
    public class SimpleWaypointsAIFSM1 : FiniteStateMachine
    {
        public List<Transform> wayPoints;
        
        public GameObject Npc { get; set; }
        public Animator Anim { get; set; }
        [SerializeField] public bool finish=false;
        public bool Finish
        {
            get { return finish; }
            set { finish = value; }
        }
        [SerializeField] public Transform player;
        public Transform Player
        {
            get { return player; }
        }
        public NavMeshAgent Agent { get; set; }

        public ThirdPersonCharacterKSModified ThirdPersonChar { get; set; }
        public AICharacterControlKSModified AICharControlKsModified { get; set; }
        
        private void Start()
        {
            //The start state
            CurrentState = new SimpleCharacterAI1.Idle(this);
            
            Npc = GetComponent<Transform>().gameObject;
            
            Anim = GetComponent<Animator>();
            Agent = GetComponent<NavMeshAgent>();
            
            ThirdPersonChar = GetComponent<ThirdPersonCharacterKSModified>();
            AICharControlKsModified = GetComponent<AICharacterControlKSModified>();
        }
        public float detectDist = 4;
        public float visAngle = 30;
       
        public void StopAI()
        {
            ThirdPersonChar.Move(Vector3.zero, false, false);
            AICharControlKsModified.SetTarget(null);
            Agent.SetDestination(Npc.transform.position);
        }
        public void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Finish"))
            {
                
                finish = true;
                GamePlayControl.Check= finish;
                Debug.Log("check:"+GamePlayControl.Check);
                Debug.Log(Finish);
            }
            
        }
       
        public void OnTriggerExit(Collider other)
        {
            if (other.gameObject.CompareTag("Finish"))
            {
                finish = false;
                Debug.Log(Finish);
            }
        }
    }

    
}

