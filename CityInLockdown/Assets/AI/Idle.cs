﻿using UnityEngine;

namespace KS.AI.FSM.SampleFSMs.SimpleCharacterAI1
{
    public class Idle : State
    {
        private SimpleWaypointsAIFSM1 _simpleWaypointsAifsm;
        public Idle(FiniteStateMachine fsm) : base(fsm)
        {
            _simpleWaypointsAifsm = ((SimpleWaypointsAIFSM1)this.FSM);
        }

        public override void Enter()
        {

            //Proceed to the next stage of the FSM state
            StateStage = StateEvent.UPDATE;
        }

        public override void Update()
        {
            if (!_simpleWaypointsAifsm.finish)
            {
                this.FSM.NextState = new SimpleCharacterAI1.Patrol(this.FSM);
                this.StateStage = StateEvent.EXIT;

            }
            _simpleWaypointsAifsm.finish = GamePlayControl.Check;
        }

        public override void Exit()
        {

        }
    }
}