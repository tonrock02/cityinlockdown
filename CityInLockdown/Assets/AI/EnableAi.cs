﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KS.AI.FSM.SampleFSMs.SimpleCharacterAI1;
public class EnableAi : MonoBehaviour
{
    public GameObject AI1;
    public GameObject AI2;
    public GameObject AI3;
    public GameObject AI4;
    public GameObject AI5;

    public int RandomAI;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("AIwalk"))
        {
            other.gameObject.GetComponent<SimpleWaypointsAIFSM1>().enabled = false;
            RandomWalkAI();
            if (GamePlayControl.Instance.peoplecount == 5)
            {
                GamePlayControl.Instance.checkAIEnded = true;
            }
            //AI2.GetComponent<SimpleWaypointsAIFSM1>().enabled = true;
        }
        if (other.CompareTag("AIwalk2"))
        {
            other.gameObject.GetComponent<SimpleWaypointsAIFSM1>().enabled = false;
            RandomWalkAI();
            if (GamePlayControl.Instance.peoplecount == 5)
            {
                GamePlayControl.Instance.checkAIEnded = true;
            }
            //AI3.GetComponent<SimpleWaypointsAIFSM1>().enabled = true;
        }
        if (other.CompareTag("AIwalk3"))
        {
            other.gameObject.GetComponent<SimpleWaypointsAIFSM1>().enabled = false;
            RandomWalkAI();
            if (GamePlayControl.Instance.peoplecount == 5)
            {
                GamePlayControl.Instance.checkAIEnded = true;
            }
            //AI4.GetComponent<SimpleWaypointsAIFSM1>().enabled = true;
        }
        if (other.CompareTag("AIwalk4"))
        {
            other.gameObject.GetComponent<SimpleWaypointsAIFSM1>().enabled = false;
            RandomWalkAI();
            if (GamePlayControl.Instance.peoplecount == 5)
            {
                GamePlayControl.Instance.checkAIEnded = true;
            }
            //AI5.GetComponent<SimpleWaypointsAIFSM1>().enabled = true;
        }
        if (other.CompareTag("AIwalk5"))
        {
            other.gameObject.GetComponent<SimpleWaypointsAIFSM1>().enabled = false;
            RandomWalkAI();
            if (GamePlayControl.Instance.peoplecount == 5)
            {
                GamePlayControl.Instance.checkAIEnded = true;
            }
        }
    }

    public void RandomWalkAI()
    {
        RandomAI = Random.Range(1, 11);
        if (RandomAI == 1 && GamePlayControl.Instance.peoplecount < 5)
        {
            AI1.GetComponent<SimpleWaypointsAIFSM1>().enabled = true;
        }
        if (RandomAI == 2 && GamePlayControl.Instance.peoplecount < 5)
        {
            AI2.GetComponent<SimpleWaypointsAIFSM1>().enabled = true;
        }
        if (RandomAI == 3 && GamePlayControl.Instance.peoplecount < 5)
        {
            AI3.GetComponent<SimpleWaypointsAIFSM1>().enabled = true;
        }
        if (RandomAI == 4 && GamePlayControl.Instance.peoplecount < 5)
        {
            AI4.GetComponent<SimpleWaypointsAIFSM1>().enabled = true;
        }
        if (RandomAI == 5 && GamePlayControl.Instance.peoplecount < 5)
        {
            AI5.GetComponent<SimpleWaypointsAIFSM1>().enabled = true;
        }
        if (RandomAI == 6 && GamePlayControl.Instance.peoplecount < 5)
        {
            AI1.GetComponent<SimpleWaypointsAIFSM1>().enabled = true;
        }
        if (RandomAI == 7 && GamePlayControl.Instance.peoplecount < 5)
        {
            AI2.GetComponent<SimpleWaypointsAIFSM1>().enabled = true;
        }
        if (RandomAI == 8 && GamePlayControl.Instance.peoplecount < 5)
        {
            AI3.GetComponent<SimpleWaypointsAIFSM1>().enabled = true;
        }
        if (RandomAI == 9 && GamePlayControl.Instance.peoplecount < 5)
        {
            AI4.GetComponent<SimpleWaypointsAIFSM1>().enabled = true;
        }
        if (RandomAI == 10 && GamePlayControl.Instance.peoplecount < 5)
        {
            AI5.GetComponent<SimpleWaypointsAIFSM1>().enabled = true;
        }
    }
}
