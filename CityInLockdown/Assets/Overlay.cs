﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Overlay : MonoBehaviour
{
    public static Overlay Instance { get; set; }
    public Image agreeoverlayImage;
    public Image rejectoverlayImage;

    private float r1;
    private float g1;
    private float b1;
    public float a1;

    private float r2;
    private float g2;
    private float b2;
    public float a2;
    // Start is called before the first frame update
    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        r1 = agreeoverlayImage.color.r;
        g1 = agreeoverlayImage.color.g;
        b1 = agreeoverlayImage.color.b;
        a1 = agreeoverlayImage.color.a;
        //
        r2 = rejectoverlayImage.color.r;
        g2 = rejectoverlayImage.color.g;
        b2 = rejectoverlayImage.color.b;
        a2 = rejectoverlayImage.color.a;
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i <= a1; i++)
        {
            a1 -= 0.003f;
        }
        for (int i = 0; i <= a2; i++)
        {
            a2 -= 0.003f;
        }
        a1 = Mathf.Clamp(a1, 0, 1f);
        AdjustColor1();
        a2 = Mathf.Clamp(a2, 0, 1f);
        AdjustColor2();
    }

    public void AdjustColor1()
    {
        Color c = new Color(r1, g1, b1, a1);
        agreeoverlayImage.color = c;
    }

    public void overlayCheck1()
    {
        //if (GamePlayControl.Instance.checkpeople == true)
        //{
            a1 += 225.0f;
            a1 = Mathf.Clamp(a1, 0, 1f);
            AdjustColor1();
        //}
    }

    public void AdjustColor2()
    {
        Color c = new Color(r2, g2, b2, a2);
        rejectoverlayImage.color = c;
    }

    public void overlayCheck2()
    {
        //if (GamePlayControl.Instance.checkpeople == true)
        //{
            a2 += 225.0f;
            a2 = Mathf.Clamp(a2, 0, 1f);
            AdjustColor2();
        //}
    }
}
