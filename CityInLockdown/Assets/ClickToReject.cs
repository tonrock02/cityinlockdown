﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickToReject : MonoBehaviour
{
    private AudioSource Stamp;

    private void Start()
    {
        Stamp = GetComponent<AudioSource>();
    }

    private void OnMouseDown()
    {
        GamePlayControl.Instance.Reject();
        Stamp.Play();
    }
}
