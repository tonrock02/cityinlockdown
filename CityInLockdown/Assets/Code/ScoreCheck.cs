﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreCheck : MonoBehaviour
{
    public int currentscore;
    public GameObject WinCon;
    public GameObject LoseCon;
    public GameObject SecretCon;

    public GameObject ButNextDay;
    public GameObject ButReturn;
    // Start is called before the first frame update
    void Start()
    {
        scorecheckUpdate();
    }
    void OnDisable()
    {
        Debug.Log("PrintOnDisable: script was disabled");
        WinCon.SetActive(false);
        LoseCon.SetActive(false);
        SecretCon.SetActive(false);
        ButNextDay.SetActive(false);
        ButReturn.SetActive(false);
    }

    void OnEnable()
    {
        scorecheckUpdate();
        Debug.Log("PrintOnEnable: script was enabled");
    }
    // Update is called once per frame
    void Update()
    {
        if (currentscore > 0)
        {
            WinCon.SetActive(true);
            ButNextDay.SetActive(true);
        }
        if (currentscore < 0)
        {
            LoseCon.SetActive(true);
            ButReturn.SetActive(true);
        }
        if (GamePlayControl.Instance.alien > 0)
        {
            SecretCon.SetActive(true);
            ButReturn.SetActive(true);
        }
        if (InfectedRandom.Instance.daycount == 5)
        {
            ButNextDay.SetActive(false);
            ButReturn.SetActive(true);
        }
    }

    public void scorecheckUpdate()
    {
        currentscore = GamePlayControl.Instance.score;
    }

    public void Nextday()
    {
        InfectedRandom.Instance.daycount += 1;
    }
}
