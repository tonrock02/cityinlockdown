﻿using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using UnityEngine;

public class RanAttribute : MonoBehaviour
{
    private GameplayManager gameplayManager;
    public int countryNum;
    public int infectedCount;
    public float temperatureCheck;
    public bool CheckStatus;

    public int checkInfected;
    public float checkTemp;

    public string currentRanCountry;
    public static RanAttribute instant;

    public bool checkInfectedTest;
    public bool checkTemperatureTest;

    void Awake()
    {
        gameplayManager = GameObject.FindObjectOfType<GameplayManager>();
        instant = this;
    }
    public void RandomCountry()
    {
        countryNum = Random.Range(1, 4);
        CountryNumber(countryNum);
        gameplayManager.UpdateCountry(currentRanCountry);
        Debug.Log(currentRanCountry);
    }

    public void RandomInfected()
    {
        infectedCount = Random.Range(0, 356);
        gameplayManager.UpdateInfected(infectedCount);
        Debug.Log(infectedCount);
    }

    public void RandomTemperature()
    {
        temperatureCheck = Random.Range(34.0f, 38.9f);
        gameplayManager.UpdateTemperature(temperatureCheck);
        Debug.Log(temperatureCheck);
    }

    private void CountryNumber(int country)
    {
        if(countryNum == 1)
        {
            currentRanCountry = "America";
        }
        if (countryNum == 2)
        {
            currentRanCountry = "Japan";
        }
        if (countryNum == 3)
        {
            currentRanCountry = "China";
        }
    }

    public void IFAgree()
    {
        CheckStatus = true;
        gameplayManager.UpdateScore(CheckStatus, infectedCount, temperatureCheck);
    }

    public void IfReject()
    {
        CheckStatus = false;
        gameplayManager.UpdateScore(CheckStatus, infectedCount, temperatureCheck);
    }
}
