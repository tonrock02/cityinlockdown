﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using KS.AI.FSM.SampleFSMs.SimpleCharacterAI1;
public class InfectedRandom : MonoBehaviour
{
    public static InfectedRandom Instance { get; set; }

    public Text usaText;
    public Text japanText;
    public Text chinaText;
    public Text ruciaText;
    public Text whitelandText;
    public Text dayText;

    public int currentusainfected;
    public int currentjapaninfected;
    public int currentchinainfected;
    public int currentruciainfected;
    public int currentwhitelandinfected;

    private int infectedusaCount;
    private int infectedjapanCount;
    private int infectedchinaCount;
    private int infectedruciaCount;
    private int infectedwhitelandCount;

    private string usaChange;
    private string japanChange;
    private string chinaChange;
    private string ruciaChange;
    private string whitelandChange;

    public int daycount = 1;
    public int currentday;

    public GameObject AI01;

    public GameObject Newpa1;
    public GameObject Newpa2;
    public GameObject Newpa3;
    public GameObject Newpa4;
    public GameObject Newpa5;
    private int ranNewpapers;
    // Start is called before the first frame update
    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        //RandomInfectedUSA();
        //RandomInfectedJapan();
        //RandomInfectedChina();
        //usaText.text = currentusainfected.ToString();
        //japanText.text = currentjapaninfected.ToString();
        //chinaText.text = currentchinainfected.ToString();
    }
    void OnDisable()
    {
        Newpa1.SetActive(false);
        Newpa2.SetActive(false);
        Newpa3.SetActive(false);
        Newpa4.SetActive(false);
        Newpa5.SetActive(false);
        Debug.Log("PrintOnDisable: script was disabled");
    }

    void OnEnable()
    {
        RandomInfectedUSA();
        RandomInfectedJapan();
        RandomInfectedChina();
        RandomInfectedRucia();
        RandomInfectedWhiteland();
        RandomNewpapers();
        usaText.text = usaChange.ToString();
        japanText.text = japanChange.ToString();
        chinaText.text = chinaChange.ToString();
        ruciaText.text = ruciaChange.ToString();
        whitelandText.text = whitelandChange.ToString();
        Debug.Log("PrintOnEnable: script was enabled");
    }
    // Update is called once per frame
    void Update()
    {
        dayText.text = currentday.ToString();
        currentday = daycount;
    }

    public void RandomInfectedUSA()
    {
        infectedusaCount = Random.Range(0, 400);
        if(infectedusaCount < 100)
        {
            usaChange = "FEW INFECTED";
        }
        if (infectedusaCount >=100 && infectedusaCount <= 300)
        {
            usaChange = "MANY INFECTED";
        }
        if (infectedusaCount > 300 )
        {
            usaChange = "CRITICAL";
        }
        currentusainfected = infectedusaCount;
        Debug.Log(infectedusaCount);
    }

    public void RandomInfectedJapan()
    {
        infectedjapanCount = Random.Range(0, 400);
        if (infectedjapanCount < 100)
        {
            japanChange = "FEW INFECTED";
        }
        if (infectedjapanCount >= 100 && infectedusaCount <= 300)
        {
            japanChange = "MANY INFECTED";
        }
        if (infectedjapanCount > 300)
        {
            japanChange = "CRITICAL";
        }
        currentjapaninfected = infectedjapanCount;
        Debug.Log(infectedjapanCount);
    }

    public void RandomInfectedChina()
    {
        infectedchinaCount = Random.Range(0, 400);
        if (infectedchinaCount < 100)
        {
            chinaChange = "FEW INFECTED";
        }
        if (infectedchinaCount >= 100 && infectedusaCount <= 300)
        {
            chinaChange = "MANY INFECTED";
        }
        if (infectedchinaCount > 300)
        {
            chinaChange = "CRITICAL";
        }
        currentchinainfected = infectedchinaCount;
        Debug.Log(infectedchinaCount);
    }

    public void RandomInfectedRucia()
    {
        infectedruciaCount = Random.Range(0, 400);
        if (infectedruciaCount < 100)
        {
            ruciaChange = "FEW INFECTED";
        }
        if (infectedruciaCount >= 100 && infectedusaCount <= 300)
        {
            ruciaChange = "MANY INFECTED";
        }
        if (infectedruciaCount > 300)
        {
            ruciaChange = "CRITICAL";
        }
        currentruciainfected = infectedruciaCount;
        Debug.Log(infectedruciaCount);
    }

    public void RandomInfectedWhiteland()
    {
        infectedwhitelandCount = Random.Range(0, 400);
        if (infectedwhitelandCount < 100)
        {
            whitelandChange = "FEW INFECTED";
        }
        if (infectedwhitelandCount >= 100 && infectedusaCount <= 300)
        {
            whitelandChange = "MANY INFECTED";
        }
        if (infectedwhitelandCount > 300)
        {
            whitelandChange = "CRITICAL";
        }
        currentwhitelandinfected = infectedwhitelandCount;
        Debug.Log(infectedwhitelandCount);
    }

    public void EnabledAI1()
    {
        AI01.GetComponent<SimpleWaypointsAIFSM1>().enabled = true;
    }

    public void RandomNewpapers()
    {
        ranNewpapers = Random.Range(1, 6);
        if (ranNewpapers == 1)
        {
            Newpa1.SetActive(true);
        }
        if (ranNewpapers == 2)
        {
            Newpa2.SetActive(true);
        }
        if (ranNewpapers == 3)
        {
            Newpa3.SetActive(true);
        }
        if (ranNewpapers == 4)
        {
            Newpa4.SetActive(true);
        }
        if (ranNewpapers == 5)
        {
            Newpa5.SetActive(true);
        }
    }
}
