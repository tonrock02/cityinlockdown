﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempCheckpoint : MonoBehaviour
{
    private AudioSource Beep;

    private void Start()
    {
        Beep = GetComponent<AudioSource>();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("AIwalk") || other.CompareTag("AIwalk2") || other.CompareTag("AIwalk3")
            || other.CompareTag("AIwalk4") || other.CompareTag("AIwalk5"))
        {
            GamePlayControl.Instance.TemperatureCheck();
            Beep.Play();
        }
    }
}
