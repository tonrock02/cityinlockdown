﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameplayManager : MonoBehaviour
{
    public Text scoreText;
    public Text countryText;
    public Text infectText;
    public Text temperatureText;

    public int currentScore;
    public string currentCountry;
    public int currentInfected;
    public float currentTemperature;

    public void UpdateCountry(string country)
    {
        currentCountry = country;
        countryText.text = "Country: " + currentCountry.ToString();
    }

    public void UpdateInfected(int infect)
    {
        currentInfected = infect;
        infectText.text = "Infect Count: " + currentInfected.ToString();
    }
    public void UpdateTemperature(float temperature)
    {
        currentTemperature = temperature;
        temperatureText.text = "Temperature: " + currentTemperature.ToString();
    }

    public void UpdateScore(bool status,int infected,float temperature)
    {
        //AGREE
        if(status == true && infected <= 100 && temperature <= 37.5)
        {
            currentScore += 5;
            scoreText.text = "Score: " + currentScore.ToString();
        }
        if (status == true && infected >= 100 && temperature <= 37.5)
        {
            currentScore += 2;
            scoreText.text = "Score: " + currentScore.ToString();
        }
        if (status == true && infected <= 100 && temperature >= 37.5)
        {
            currentScore += 1;
            scoreText.text = "Score: " + currentScore.ToString();
        }
        if (status == true && infected >= 100 && temperature >= 37.5)
        {
            currentScore -= 5;
            scoreText.text = "Score: " + currentScore.ToString();
        }
        //REJECT
        if (status == false && infected >= 100 && temperature >= 37.5)
        {
            currentScore += 5;
            scoreText.text = "Score: " + currentScore.ToString();
        }
        if (status == false && infected <= 100 && temperature >= 37.5)
        {
            currentScore += 2;
            scoreText.text = "Score: " + currentScore.ToString();
        }
        if (status == false && infected >= 100 && temperature <= 37.5)
        {
            currentScore += 1;
            scoreText.text = "Score: " + currentScore.ToString();
        }
        if (status == false && infected <= 100 && temperature <= 37.5)
        {
            currentScore -= 5;
            scoreText.text = "Score: " + currentScore.ToString();
        }
    }
}
