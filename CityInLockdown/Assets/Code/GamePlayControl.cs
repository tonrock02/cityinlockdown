﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using KS.AI.FSM.SampleFSMs.SimpleCharacterAI1;

public class GamePlayControl : MonoBehaviour
{
    public bool counCheckPoint;
    public static bool Check;
    public static GamePlayControl Instance { get; set; }
    public SimpleWaypointsAIFSM1 fsm;
    public TextMeshPro countryText;
    public TextMeshPro temperatureText;

    private int infectcountUSA;
    private int infectcountJapan;
    private int infectcountChina;
    private int infectcountRucia;
    private int infectcountWhiteland;
    private int currentinfected;

    public int countryNum;
    public string currentrandomcountry;

    public float temprandom;
    public float currenttemperature;

    public int score;
    public int peoplecount;
    public bool buttonCheck1;
    public bool buttonCheck2;
    public bool checkpeople;

    public int alien;

    public GameObject dayendbutton;
    //-----------Overlay----------
    public Image agreeoverlayImage;
    public Image rejectoverlayImage;

    private float r1;
    private float g1;
    private float b1;
    public float a1;

    private float r2;
    private float g2;
    private float b2;
    public float a2;

    public bool AlienCome;
    public bool checkAIEnded;

    public GameObject gameplayPanel;
    public GameObject resultPanel;
    // Start is called before the first frame update
    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        //infectcountUSA = InfectedRandom.Instance.currentusainfected;
        //infectcountJapan = InfectedRandom.Instance.currentjapaninfected;
        //infectcountChina = InfectedRandom.Instance.currentchinainfected;
        //Debug.Log(infectcountUSA);
        //Debug.Log(infectcountJapan);
        //Debug.Log(infectcountChina);
        //RandomCountry();
        //RandomTemperature();
        r1 = agreeoverlayImage.color.r;
        g1 = agreeoverlayImage.color.g;
        b1 = agreeoverlayImage.color.b;
        a1 = agreeoverlayImage.color.a;
        //
        r2 = rejectoverlayImage.color.r;
        g2 = rejectoverlayImage.color.g;
        b2 = rejectoverlayImage.color.b;
        a2 = rejectoverlayImage.color.a;
        //
    }
    void OnDisable()
    {
        countryText.text = "";
        temperatureText.text = "";
        peoplecount = 0;
        Debug.Log(peoplecount);
        Debug.Log("PrintOnDisable: script was disabled");
    }

    void OnEnable()
    {
        infectcountUSA = InfectedRandom.Instance.currentusainfected;
        infectcountJapan = InfectedRandom.Instance.currentjapaninfected;
        infectcountChina = InfectedRandom.Instance.currentchinainfected;
        infectcountRucia = InfectedRandom.Instance.currentruciainfected;
        infectcountWhiteland = InfectedRandom.Instance.currentwhitelandinfected;
        Debug.Log(infectcountUSA);
        Debug.Log(infectcountJapan);
        Debug.Log(infectcountChina);
        Debug.Log(infectcountRucia);
        Debug.Log(infectcountWhiteland);
        RandomCountry();
        RandomTemperature();
        checkpeople = true;
        dayendbutton.gameObject.SetActive(false);
        Debug.Log("PrintOnEnable: script was enabled");
    }
    // Update is called once per frame
    void Update()
    {
        if(currentrandomcountry == "U.S.A.")
        {
            currentinfected = infectcountUSA;
        }
        if (currentrandomcountry == "Layland")
        {
            currentinfected = infectcountJapan;
        }
        if (currentrandomcountry == "West Tawan")
        {
            currentinfected = infectcountChina;
        }
        if (currentrandomcountry == "Rucia")
        {
            currentinfected = infectcountRucia;
        }
        if (currentrandomcountry == "White Land")
        {
            currentinfected = infectcountWhiteland;
        }
        if (peoplecount == 5)
        {
            checkpeople = false;
            dayendbutton.gameObject.SetActive(true);
        }
        for (int i = 0; i <= a1; i++)
        {
            a1 -= 0.003f;
        }
        for (int i = 0; i <= a2; i++)
        {
            a2 -= 0.003f;
        }
        a1 = Mathf.Clamp(a1, 0, 1f);
        AdjustColor1();
        a2 = Mathf.Clamp(a2, 0, 1f);
        AdjustColor2();
    }

    public void Checkcountry()
    {
        if(peoplecount < 5 && counCheckPoint == true)
        {
            countryText.text = currentrandomcountry.ToString();
            buttonCheck1 = true;
        }
    }

    public void RandomCountry()
    {
        countryNum = Random.Range(1, 6);
        CountryNumber();
    }

    public void CountryNumber()
    {
        if (countryNum == 1)
        {
            currentrandomcountry = "U.S.A.";
        }
        if (countryNum == 2)
        {
            currentrandomcountry = "Layland";
        }
        if (countryNum == 3)
        {
            currentrandomcountry = "West Tawan";
        }
        if (countryNum == 4)
        {
            currentrandomcountry = "Rucia";
        }
        if (countryNum == 5)
        {
            currentrandomcountry = "White Land";
        }
    }

    public void RandomTemperature()
    {
        if (peoplecount < 5)
        {
            temprandom = Random.Range(34.0f, 40.0f);
            currenttemperature = temprandom;
        }
    }

    public void TemperatureCheck()
    {
        if(peoplecount < 5)
        {
            temperatureText.text = currenttemperature.ToString("0.##");
            buttonCheck2 = true;
        }
    }

    public void Agree()
    {
        
        if (buttonCheck1 == true && buttonCheck2 == true && checkpeople == true)
        {
            if (currenttemperature <= 37.0f)
            {
                score += 25;
            }
            if (currenttemperature >= 37.0f)
            {
                score -= 25;
            }
            if(currentinfected <= 100)
            {
                score += 5;
            }
            if (currentinfected > 100 && currentinfected <= 300)
            {
                score += 3;
            }
            if (currentinfected > 300)
            {
                score -= 5;
            }
            if (peoplecount < 5)
            {
                RandomCountry();
                RandomTemperature();
                Debug.Log(peoplecount);
                peoplecount += 1;
            }
            if (AlienCome == true)
            {
                score -= 1000;
                alien += 1;
            }
            Check = false;
            counCheckPoint = false;
            AlienCome = false;
            overlayCheck1();
            buttonCheck1 = false;
            buttonCheck2 = false;
            countryText.text = "";
            temperatureText.text = "";
        }
        Debug.Log(score);
    }

    public void Reject()
    {
        
        if (buttonCheck1 == true && buttonCheck2 == true && checkpeople == true)
        {
            if (currenttemperature <= 37.0f)
            {
                score -= 25;
            }
            if (currenttemperature >= 37.0f)
            {
                score += 25;
            }
            if (currentinfected <= 100)
            {
                score -= 5;
            }
            if (currentinfected > 100 && currentinfected <= 300)
            {
                score -= 3;
            }
            if (currentinfected > 300)
            {
                score += 5;
            }
            if (peoplecount < 5)
            {
                RandomCountry();
                RandomTemperature();
                Debug.Log(peoplecount);
                peoplecount += 1;
            }
            Check = false;
            counCheckPoint = false;
            AlienCome = false;
            overlayCheck2();
            buttonCheck1 = false;
            buttonCheck2 = false;
            countryText.text = "";
            temperatureText.text = "";
        }
        Debug.Log(score);
    }

    public void AdjustColor1()
    {
        Color c = new Color(r1, g1, b1, a1);
        agreeoverlayImage.color = c;
    }

    public void overlayCheck1()
    {
        //if (GamePlayControl.Instance.checkpeople == true)
        //{
        a1 += 225.0f;
        a1 = Mathf.Clamp(a1, 0, 1f);
        AdjustColor1();
        //}
    }

    public void AdjustColor2()
    {
        Color c = new Color(r2, g2, b2, a2);
        rejectoverlayImage.color = c;
    }

    public void overlayCheck2()
    {
        //if (GamePlayControl.Instance.checkpeople == true)
        //{
        a2 += 225.0f;
        a2 = Mathf.Clamp(a2, 0, 1f);
        AdjustColor2();
        //}
    }

    public void CheckAI()
    {
        if (checkAIEnded == true)
        {
            gameplayPanel.SetActive(false);
            resultPanel.SetActive(true);
            checkAIEnded = false;
        }
    }
}
