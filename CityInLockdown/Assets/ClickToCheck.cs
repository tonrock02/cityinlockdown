﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickToCheck : MonoBehaviour
{
    private AudioSource FlipSound;

    private void Start()
    {
        FlipSound = GetComponent<AudioSource>();
    }

    private void OnMouseDown()
    {
        GamePlayControl.Instance.Checkcountry();
        FlipSound.Play();
    }
}
