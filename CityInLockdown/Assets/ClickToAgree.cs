﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickToAgree : MonoBehaviour
{
    private AudioSource Stamp;

    private void Start()
    {
        Stamp = GetComponent<AudioSource>();
    }

    private void OnMouseDown()
    {
        GamePlayControl.Instance.Agree();
        Stamp.Play();
    }
}
