﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonGameApplicationManager : Singleton<SingletonGameApplicationManager>
{
    public bool IsOptionMenuActive
    {
        get { return _isOptionMenuActive; }
        set { _isOptionMenuActive = value; }
    }
    protected bool _isOptionMenuActive = false;

    public bool IsCreditActive
    {
        get { return _isCreditActive; }
        set { _isCreditActive = value; }
    }
    protected bool _isCreditActive = false;

    public int DifficultyLevel{get;set;}
    public bool MusicEnabled { get; set; } = true;
    public bool SFXEnabled { get; set; } = true;
}
