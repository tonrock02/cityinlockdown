﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
public class MainMenuControlScriptBGMSFX : MonoBehaviour, IPointerEnterHandler
{
    [SerializeField] Button _startButton;
    [SerializeField] Button _optionsButton;
    [SerializeField] Button _creditButton;
    [SerializeField] Button _exitButton;
    
    // Start is called before the first frame update

    AudioSource audiosourceButtonUI;
    [SerializeField] AudioClip audioclipSelect;
    void Start()
    {
        this.audiosourceButtonUI = this.gameObject.AddComponent<AudioSource>();
        this.audiosourceButtonUI.outputAudioMixerGroup = SingletonSoundManager.Instance.Mixer.FindMatchingGroups("UI")[0];
        SetupButtonsDelegate();

        if (!SingletonSoundManager.Instance.BGMSource.isPlaying)
            SingletonSoundManager.Instance.BGMSource.Play();
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (audiosourceButtonUI.isPlaying)
            audiosourceButtonUI.Stop();

        audiosourceButtonUI.PlayOneShot(audioclipSelect);
    }
    void SetupButtonsDelegate()
    {
        _startButton.onClick.AddListener(delegate { StartButtonClick(_startButton); });
        _optionsButton.onClick.AddListener(delegate { OptionsButtonClick(_optionsButton); });
        _exitButton.onClick.AddListener(delegate { ExitButtonClick(_exitButton); });
        _creditButton.onClick.AddListener(delegate { CreditButtonClick(_creditButton); });
    }

    public void StartButtonClick(Button button)
    {
        SceneManager.LoadScene("GamePlay");
    }
    public void OptionsButtonClick(Button button)
    {
        if (!SingletonGameApplicationManager.Instance.IsOptionMenuActive)
        {
            SceneManager.LoadScene("OptionScene", LoadSceneMode.Additive);
            SingletonGameApplicationManager.Instance.IsOptionMenuActive = true;
        }
    }
    public void ExitButtonClick(Button button)
    {
        Debug.Log("Exiting....");
        Application.Quit();
    }
    public void CreditButtonClick(Button button)
    {
        if (!SingletonGameApplicationManager.Instance.IsCreditActive)
        {
            SceneManager.LoadScene("CreditScene", LoadSceneMode.Additive);
            SingletonGameApplicationManager.Instance.IsCreditActive = true;
        }
    }
}
