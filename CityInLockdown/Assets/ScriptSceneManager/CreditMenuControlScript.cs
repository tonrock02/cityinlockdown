﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CreditMenuControlScript : MonoBehaviour
{
    [SerializeField] Button _backButton;
    // Start is called before the first frame update
    void Start()
    {
        _backButton.onClick.AddListener(delegate { BackToMainMenuButtonClick(_backButton); });
    }

    public void BackToMainMenuButtonClick(Button button)
    {
        SceneManager.UnloadSceneAsync("CreditScene");
        SingletonGameApplicationManager.Instance.IsCreditActive = false;
    }
}
